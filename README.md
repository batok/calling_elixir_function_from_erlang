# You know elixir language and are starting with erlang language.
# What if you could use that fancy function you wrote in elixir from erlang ?
# Just follow these easy instructions... 

# Step 1.
## Clone this repo ( just to have this README in your filesystem ).

# Step 2.
## Get into the repo
```
$ cd calling_elixir_function_from_erlang
```
# Step 3.
## Create a simple project with elixir
```
$ mix new simple
```
# Step 4. 
## Compile it
```
$ cd simple
$ mix compile
```
Note: your Simple module now has the hello function which return :world

# Step 5.
## Check if it's ok
````bash
$ iex -S mix
iex(1)> Simple.hello
```

If :world is displayed your elixir module is ready.

# Step 6.
## Start erl which seems like iex, because ... iex is erl under the hood.
```
$ erl
```

# Step 7.
## Inside the REPL add the path of beam file(s) of the Simple project.
```erlang 
code:add_path("PathToYourMixProject/_build/dev/lib/simple/ebin").
```

# Step 8.
## Final step, run the function ( also inside the REPL ).
```erlang 
'Elixir.Simple':hello().
```

Note: If you place the code in step 7 in an .erlang file , if that file is present in the path where you type erl that code will be run, in this case adding the path of your elixir module.  This way you don't need to do step 7 next time, if you quit erl by typing ctrl-c twice. Cheers
